<img src="http://blog.legacyteam.info/wp-content/uploads/2014/10/laravel-logo-white.png" width="280px">

Laravel es un framework de aplicaciones web con una sintaxis expresiva y elegante. Creemos que el desarrollo debe ser una experiencia agradable y creativa para ser verdaderamente satisfactorio. Laravel intenta aliviar el dolor del desarrollo al facilitar las tareas comunes utilizadas en la mayoría de los proyectos web, como la autenticación, el enrutamiento, las sesiones, las colas y el almacenamiento en caché.

## DESCARGA E INSTALACIÓN DE CLOUDHOSTELS

1. Descargar e Instalar [PHP](http://php.net/downloads.php).
2. Descargar e Instalar [XAMPP](https://www.apachefriends.org/download.html).
3. Descargar e Instalar [Composer](https://getcomposer.org/).
4. Install Mcrypt PHP Extension:
 - <img src="http://deluge-torrent.org/images/apple-logo.gif" height="17"> **Mac OS X**: `brew install php55-mcrypt`.
 - <img src="https://lh5.googleusercontent.com/-2YS1ceHWyys/AAAAAAAAAAI/AAAAAAAAAAc/0LCb_tsTvmU/s46-c-k/photo.jpg" height="17"> **Ubuntu**: `sudo apt-get install php5-mcrypt`.
5. Clonar el Proyecto desde consola con el comando git clone https://bitbucket.org/viistorrr/api-cloudhostels.git
5. Install dependencies via `composer install`.
6. Migrar la Base de Datos de CloudHostels con ejecutando el comando php artisan migrate en consola
7. Run `php artisan serve --port 3000`, then go to `http://localhost:3000`.
