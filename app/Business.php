<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Gallery;

class Business extends Model {

	protected $table = 'business';
	public $timestamps = true;
	protected $fillable = array('name', 'address', 'category_id', 'web', 'twitter', 'facebook', 'instagram', 'picture', 'code');
	#protected $visible = array('name', 'address', 'category_id', 'web', 'twitter', 'facebook', 'instagram', 'picture');
    protected $hidden = ['code'];

    public function gallery(){
        return $this->hasMany('App\Gallery', 'business_id', 'id');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
}