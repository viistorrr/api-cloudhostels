<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'servicios';
	public $timestamps = true;
	protected $fillable = array('nombre');

    public function business(){
        return $this->hasMany('App\Business');
    }

    public function scopeCity($query, $city){
        if($city){
            return $query->where('city_id', $city);
        }
        return $query;
    }
}