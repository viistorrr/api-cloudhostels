<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Companion extends Model {

	protected $table = 'companions';
	public $timestamps = true;

	public function client(){
		return $this->belongsTo('App\Client');
	}
}
