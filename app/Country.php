<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	protected $table = 'countries';
	public $timestamps = true;
	protected $fillable = array('code','name','gentilicio');

}