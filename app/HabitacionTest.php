<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HabitacionTest extends Model {

	protected $table = 'accommodation';
	public $timestamps = true;
	protected $fillable = array('name');

}