<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Business;
use App\Transaction;
use App\User;
use App\Category;

class BusinessController extends Controller {
  private $thumbSizeMedium = 600;

  public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $user = User::find(\Input::get('user.sub'));
    //return json_encode(\Input::all());
    $category = Category::with(['business' => function($query) use($user){
      return $query->where('city_id', $user->city_id);
    }])->get();
    return $category->toJson();
    //old query without category
    /*$business = Business::with('gallery', 'category')->get();
    return $business->toJson();*/
  }

  public function byCategory($id)
  {
    $category = Category::where('id', $id)->with('business.gallery')->first();

    return $category->toJson();

    $business = Business::where('category_id', $id)->with('gallery', 'category')->get();
    return $business->toJson();
  }



  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $business = Business::with('gallery')->where('id', $id)->firstOrFail();
    $business->gallery->each(function($i){
      $i->thumb = 'http://app.expectalia.com/thumb/phpThumb.php?src=/uploads/galeria/'.$i->image.'&w=200&h=200&far=1&bg=FFFFFF&q=92';
    });
    return $business->toJson();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function load(Request $request)
  {
    $user = User::findOrFail($request['user']['sub']);
    $business = Business::findOrFail($request->get('id'));

    /* Picture Upload */
    $path = 'uploads/bills/' . $user->id . '/';
    $destinationPath = public_path($path);

    $tempfile = $request->file('file');
    $image = \Image::make($tempfile->getRealPath());

    if($tempfile){
        $filename = pathinfo($tempfile->getClientOriginalName(), PATHINFO_FILENAME);
        $filename = uniqid()."__".str_slug($filename) .".". $tempfile->getClientOriginalExtension();

        // Guardar Original
        $tempfile->move($destinationPath, $filename);
        $image->resize($this->thumbSizeMedium, $this->thumbSizeMedium, function($c){
            $c->aspectRatio();
            $c->upsize();
        });

        if($image->save($destinationPath.$filename)){
            $points = (int)($request['amount']/1000);

            $t = new Transaction;
            $t->fill($request->all());
            $t->user_id = $user->id;
            $t->business_id = $business->id;
            $t->type = 'load';
            $t->points = $points;
            $t->picture = asset($path.$filename);

            if($t->save()){
              return response()->json([
                  'error' => false,
                  'message' => 'Felicitaciones, has cargado '.$points.' puntos.',
              ], 200);
            }
        } #end if save
    }#end if
    /* :Picture Upload  */
  }

  public function getPoints()
  {
    $request = \Input::all();
    $user = User::findOrFail($request['user']['sub']);

    return response()->json([
        'error' => false,
        'points' => ($user->balance) ? $user->balance : 0,
    ], 200);

  }



}

?>