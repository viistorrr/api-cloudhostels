<?php namespace App\Http\Controllers;

use App\Cabana;
use Illuminate\Http\Request;
class CabanaController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $habitaciones = Cabana::get();

    return $habitaciones->toJson();//retorna todos lo que viene de la tabla de la DB
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $habitaciones = new Cabana;
      $habitaciones->fill($request->all());

    if($habitaciones->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el registro.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el registro.',
      ], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $habitaciones = Cabana::findOrFail($id);
      return $habitaciones->toJson();
      
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $habitaciones = Cabana::findOrFail($id);
      $habitaciones->fill($request->only(['code','name','gentilicio']));

    if($habitaciones->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $habitaciones = Cabana::findOrFail($id);

    if($habitaciones->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }

  public function cabanadatein($id){
    $cabana_date_in = Cabana::where("cabanas.id","!=",$id)
        ->get();
    return json_encode($cabana_date_in);
  }

  public function cabanadateout($id){
    $cabana_date_out = Cabana::where("cabanas.id","!=",$id)
        ->get();
    return json_encode($cabana_date_out);
  }

}

?>