<?php namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
class ClientController extends Controller {

  /**
   * Display a list of clients.
   *
   * @return Response
   */
  public function index()
  {
    $clients = Client::get();

    return $clients->toJson();

    /*
      public function index()
  {
    $items = Client::join("countries", function($join){
      $join->on("clients.country_id","=","countries.id");
    })
    ->get([
      "clients.*",
      "countries.name as country"
    ]);
    */


  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    //return json_encode($request->all());
    $s = new Client;
    $s->fill($request->all());

    if($s->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el Cliente.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el Cliente.',
      ], 404);
    }
  }

  /**
   * Display the specified client.
   *
   * @param  int  $documento
   * @return Response
   */
  public function show($documento)
  {
    $client = Client::whereDocument($documento)->first();

    return $client->toJson();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $documento)
  {
    $client = Client::whereDocument($documento)->first();
    $client->fill($request->only(['name',
                                    'document',
                                    'phone',
                                    'email']));

    if($client->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado correctamente.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el Cliente.',
    ], 404);
  }

  /**
   * Delere the specified Client from storage.
   *
   * @param  int  $documento
   * @return Response
   */
  public function destroy($documento)
  {
    $category = Client::findOrFail($documento);

    if($category->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Cliente eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el Cliente.',
    ], 404);
  }

}

?>