<?php namespace App\Http\Controllers;

use App\ClientTest;
use Illuminate\Http\Request;
class ClientTestController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $items = ClientTest::get();
    return $items->toJson();//retorna todos lo que viene de la tabla de la DB
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function store(){
    
  }

  //Método de Prueba Unitaia
  public function insertarTest()
  {
    $s = new ClientTest;

    $validate_document = ClientTest::whereDocument("11111")->first();
    
    if(is_null($validate_document)){
      $s->name = "Prueba Ejecución Método";
      $s->document = "11111";
      $s->phone = "99999999";
      $s->email = "pruebaunitaria@mail.com";
      if($s->save()){
        return response()->json([
            'error' => false,
            'message' => 'Prueba Ejecutada Correctamente.',
        ], 200);
    }
    return response()->json([
        'error' => true,
        'message' => 'Error: No se pudo ejecutar la Prueba Unitaria porque los Datos están Duplicados',
     ], 404);
     }else{
        return response()->json([
         'error' => true,
         'message' => 'Error: No se pudo ejecutar la Prueba Unitaria porque los Datos están Duplicados',
      ], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $client = ClientTest::findOrFail(99999999);

    return $client->toJson();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $category = ClientTest::findOrFail($id);
    $category->fill($request->only(['name',
                                    'document',
                                    'phone',
                                    'email']));

    if($category->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function eliminarTest()
  {
    $category = ClientTest::findOrFail(99999999);

    if($category->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }
}
?>