<?php namespace App\Http\Controllers;

use App\Companion;
use Illuminate\Http\Request;
class CompanionController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $companions = Companion::join("reservations", function($join){
      $join->on("reservations.id","=","companions.reservation_id");
    })->get([
      "companions.client_id"
    ]);

    return $companions->toJson();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    /*$c = new Service;
    $c->fill($request->all());

    if($c->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado la categoria.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar la categoria.',
      ], 404);
    }*/
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $companions = Companion::join("reservations", function($join){
      $join->on("reservations.id","=","companions.reservation_id");
    })
        ->join("clients", function($join){
          $join->on("clients.id","=","companions.client_id");
        })
        ->where("companions.reservation_id","=",$id)
        ->get([
          "companions.client_id",
          "clients.name",
          "clients.lastname1 as client_lastname1",
          "clients.lastname2 as client_lastname2"
    ]);

    return $companions->toJson();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    /*$category = Service::findOrFail($id);
    $category->fill($request->only('name'));

    if($category->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);*/
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  /*  $category = Service::findOrFail($id);

    if($category->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);*/
  }

}

?>