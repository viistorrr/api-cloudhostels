<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Reservation;
use App\RevenueManagement;
use App\Cabana;
use App\Price;
use App\Client;
use App\Http\Middleware;


class DashboardController extends Controller {

	/**
	 * Display a calendar list of reservations.
	 *
	 * @return Response
	 */
	public function index()
    {
    	//return json_encode(\Auth()->user);
    	
    	$reservations = Reservation::select([
			'reservations.id',
			\DB::raw('UPPER (CONCAT("MT0",reservations.id,":", " ",clients.name, " ", clients.lastname1," - ",accommodation.name, " - ", reservations.status)) AS title'),
			"reservations.id",
			"reservations.date_in as start",
			"reservations.date_out as end",
			"reservations.status",
			"reservations.backgroundColor",
			"accommodation.name",
			"clients.name as client_name",
			"clients.lastname1 as client_lastname1",
			"clients.lastname2 as client_lastname2"
		])->join('clients', function($join){
			$join->on("reservations.client_id","=","clients.id");
		})->join('accommodation', function($join) {
			$join->on("reservations.accommodation_id", "=", "accommodation.id");
		})
			->get();//El Calendario debe mostrar TODAS LAS RESERVAS, inclusive las que ya se les realizó CheckOut para tener control Total

		return $reservations->toJson();
	}

	public function today()
	{
		$date=date('Y-m-d');
		//return json_encode($date);
		$reservations = Reservation::select([
			'reservations.id',
			\DB::raw('UPPER (CONCAT(clients.name, " ", clients.lastname1," ", clients.lastname2," - ",accommodation.name, " - ", reservations.status)) AS title'),
			"reservations.date_in as start",
			"reservations.date_out as end",
			"reservations.status",
			"reservations.backgroundColor",
			"accommodation.name",
			"clients.name as client_name",
			"clients.lastname1 as client_lastname1",
			"clients.lastname2 as client_lastname2"
		])->join('clients', function($join){
			$join->on("reservations.client_id","=","clients.id");
		})->join('accommodation', function($join) {
			$join->on("reservations.accommodation_id", "=", "accommodation.id");
		})->where("reservations.date_in","=",$date)
			->get();//El Calendario debe mostrar TODAS LAS RESERVAS, inclusive las que ya se les realizó CheckOut para tener control Total

		return $reservations->toJson();
	}

	//Reporte SIRE route
	/**
	 * TODO:
	 * Generar Reporte Diario en Excel de los EXTRANJEROS que ingresan al Hotel
	 * Valida la Fecha del Día actual y las personas registradas en reservas
	 * que ya les hayan realizado CheckIN/CheckOUT se generan en el reporte
	 */
	public function sire()
	{
		//Asi funciona pero solo trae el reporte del titular de la reserva
		  /*$today = date('Y-m-d');
		$titular = \DB::table('reservations')
				->join("clients", function($join){
					$join->on("clients.id","=","reservations.client_id");
					})->join("countries", function($join){
					$join->on("countries.id","=","clients.country_id");
				})->where("countries.name","!=",'COLOMBIA')
					->where("reservations.status","!=","PENDIENTE")
					->where("reservations.status","!=","CANCELADA")
					->where("reservations.date_in","=",$today)
					->orWhere("reservations.date_out","=",$today)
					->orderBy('reservations.id','DESC')
					->get([
						"reservations.status",
						"reservations.date_in",
						"reservations.date_out",
						"clients.*",
						"countries.code"
					]);*/


		//*****Asi FUNCIONA: trae los Acompañantes EXTRANJEROS, Falta que traiga además el titular de la reserva
		/*$today = date('Y-m-d');
		$reservations = Client::join("companions", function($join){
			$join->on("clients.id","=","companions.client_id");
		})->join("countries", function($join){
			$join->on("countries.id","=","clients.country_id");
		})->join("reservations", function($join){
			$join->on("reservations.id","=","companions.reservation_id");
		})->where("countries.name","!=",'COLOMBIA')
			->where("reservations.status","!=","PENDIENTE")
			->where("reservations.status","!=","CANCELADA")
			->where("reservations.date_in","=",$today)
			->orWhere("reservations.date_out","=",$today)
			->orderBy('reservations.id','DESC')
			->get([
				"reservations.status",
				"reservations.date_in",
				"reservations.date_out",
				"clients.*",
				"countries.code",
				"companions.client_id"
			]);
		return $reservations->toJson();*/

		//
		$today = date('Y-m-d');
		$reservations = \DB::table('clients')->join("companions", function($join){
			$join->on("clients.id","=","companions.client_id");
		})->join("countries", function($join){
			$join->on("countries.id","=","clients.country_id");
		})->join("reservations", function($join){
			$join->on("reservations.id","=","companions.reservation_id");
		})->where("countries.name","!=",'COLOMBIA')
			->where("reservations.status","!=","PENDIENTE")
			->where("reservations.status","!=","CANCELADA")
			->where("reservations.date_in","=",$today)
			->orWhere("reservations.date_out","=",$today)
			->orderBy('reservations.id','DESC')

			->get([
				"reservations.status",
				"reservations.date_in",
				"reservations.date_out",
				"clients.*",
				"countries.code",
				"companions.client_id as companion_id",
				"reservations.client_id"
			]);
		return json_encode($reservations);
	}

	//Reporte SIRE route
	
	/**
	 * TODO:
	 * Generar Reporte Mensual en Excel Reveneu Management
	 */
	public function revenuemanagement()
	{
		$day = date('d');
		$month = date('m');
		$year = date('Y');
		$today = date('Y-m-d');

		RevenueManagement::where('year',$year = date('Y'))
							->where('month',$month = date('m'))
							->delete();

		/*TEST PARA TODAS LAS FECHAS*/
		for($i=1;$i<=$day;$i++)
		{
		$revenue = new RevenueManagement;

    	$revenue->day = $i;//Dia
    	$revenue->month = date('m');//Mes
    	$revenue->year = date('Y');//Año

    	$today_i = $year."-".$month."-".$i;

    	$revenue->total_cabanas_disponibles = Cabana::count();

    	$revenue->cabanas_ocupadas = 
    			Reservation::where("reservations.date_in","=",$today_i)
							->orWhere("reservations.date_out","=",$today_i)
							->count();

		$revenue->cabanas_para_ocupar = 
				Cabana::count() - Reservation::where("reservations.date_in","=",$today_i)
				->orWhere("reservations.date_out","=",$today_i)->count();

		$revenue->huespedes_alojados = Client::join("reservations", function($join){
			$join->on("clients.id","=","reservations.client_id");
		})->join("companions",function($join){
			$join->on("companions.reservation_id","=","reservations.id");
		})
		  ->where("reservations.date_in","=",$today_i)
		  ->orWhere("reservations.date_out","=",$today_i)
		  ->count();

		  $revenue->tarifa = Reservation::join("prices", function($join){
		  	$join->on("reservations.price_id","=","prices.id");
		  })
		  ->where("reservations.date_in","=",$today_i)
		  ->orWhere("reservations.date_out","=",$today_i)
		  ->max("prices.value");

		  if((($revenue->cabanas_ocupadas)!=null) || (($revenue->cabanas_ocupadas) != 0)){
		  		$revenue->venta_hab = $revenue->huespedes_alojados * $revenue->tarifa;
		  		$revenue->porc_ocupacion = ($revenue->cabanas_ocupadas / $revenue->total_cabanas_disponibles)*100;
		  		$revenue->i_alojamiento = $revenue->huespedes_alojados / $revenue->cabanas_ocupadas;
		  		$revenue->tarifa_prom_cabana = $revenue->venta_hab / $revenue->cabanas_ocupadas;
		  		$revenue->rev_v_par = $revenue->venta_hab / $revenue->cabanas_para_ocupar;
			}

    	$revenue->save();
		}
	}

	public function getrevenuemanagement(){
		$revenue = RevenueManagement::get();
		return json_encode($revenue);
	}


}
