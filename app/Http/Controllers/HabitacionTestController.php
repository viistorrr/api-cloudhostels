<?php namespace App\Http\Controllers;

use App\HabitacionTest;
use Illuminate\Http\Request;
class HabitacionTestController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $habitaciones = HabitacionTest::get();

    return $habitaciones->toJson();//retorna todos lo que viene de la tabla de la DB
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $s = new HabitacionTest;
    $s->fill($request->all());

    if($s->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el registro.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el registro.',
      ], 404);
    }
  }

  //Método de Prueba
  public function insertarHabitacion()
  {
    $s = new HabitacionTest;

    $validate_hab = HabitacionTest::whereName("HABITACION 1")->first();

    if(is_null($validate_hab)){
          $s->fill($request->all());

        if($s->save()){
          return response()->json([
              'error' => false,
              'message' => 'Se ha guardado el registro.',
          ], 200);
        }else{
          return response()->json([
              'error' => true,
              'message' => 'Error al guardar el registro.',
          ], 404);
        }
    }else{
      return response()->json([
              'error' => true,
              'message' => 'Error: No se pudo ejecutar la Prueba Unitaria porque los Datos están Duplicados.',
          ], 404);
    }

    
  }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $hab = HabitacionTest::findOrFail(99999999);

    return $hab->toJson();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $habitaciones = HabitacionTest::findOrFail(9999999);
    $habitaciones->fill($request->only(['name']));

    if($habitaciones->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  public function eliminarHabitacion()
  {
    $category = HabitacionTest::findOrFail(99999999);

    if($category->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }

}

?>