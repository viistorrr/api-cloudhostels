<?php namespace App\Http\Controllers;

use App\Price;
use Illuminate\Http\Request;
class PriceController extends Controller {

  /**
   * Display a listing of prices.
   *
   * @return Response
   */
  public function index()
  {
    $tarifas = Price::get();

    return $tarifas->toJson();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $price = new Price;
    $price->fill($request->all());

    if($price->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el registro.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el registro.',
      ], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $price = Price::findOrFail($id);
      $price->fill($request->only(['name','value']));

    if($price->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $price = Price::findOrFail($id);

    if($price->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }

}

?>