<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Reservation;
use App\Companion;
use App\Consumo;
use App\Cabana;

class ReservationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reservations = Reservation::join("accommodation", function($join){
			$join->on('accommodation.id',"=","reservations.accommodation_id");
		})
			->join("clients", function($join){
				$join->on("clients.id","=","reservations.client_id");
			})
			->with('companions.client', 'client')
			->orderBy('reservations.id','DESC')
			->get([
				"reservations.*",
				"accommodation.name as cabana_name",
				"clients.name",
				"clients.lastname1 as lastname",
				"clients.lastname2"
			]);
		return json_encode($reservations);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
  	{
  		$inputs = $request->all();
    	$r = new Reservation;
    	$r->client_id = $inputs['reservation']['client_id'];
    	$r->date_in = $inputs['reservation']['date_in'];
    	$r->date_out = $inputs['reservation']['date_out'];
    	$r->accommodation_id = $inputs['reservation']['accommodation_id'];
    	$r->abono = $inputs['reservation']['abono'];
    	/*if(isset($inputs['reservation']['abono'])){
			$r->abono = 0;
		}else{
			$r->abono = $inputs['reservation']['abono'];
		}*/

    	$r->price_id = $inputs['reservation']['price_id'];
    	$reservation->annotations = $inputs['reservation']['annotations'];

    	if($r->save()){
			$c = new Companion;
			$c->reservation_id = $r->id;
			$c->client_id = $inputs['reservation']['client_id'];
			$c->save();
    		foreach ($inputs['companions'] as $key => $companion) {
    			$c = new Companion;
    			$c->reservation_id = $r->id;
    			$c->client_id = $companion['id'];
    			$c->save();
    		}
      		return response()->json([
          		'error' => false,
          		'message' => 'Se ha guardado la reserva.',
      		], 200);
    	}else{
      		return response()->json([
          		'error' => true,
          		'message' => 'Error al guardar la categoria.',
      		], 404);
    	}
  	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$inputs = $request->all();
		$reservation = Reservation::findOrFail($id);
		$reservation->client_id = $inputs['reservation']['client_id'];
		$reservation->date_in = $inputs['reservation']['date_in'];
		$reservation->date_out = $inputs['reservation']['date_out'];
		$reservation->accommodation_id = $inputs['reservation']['accommodation_id'];
		$reservation->abono = $inputs['reservation']['abono'];
		$reservation->price_id = $inputs['reservation']['price_id'];
		$reservation->annotations = $inputs['reservation']['annotations'];

		if($reservation->save()){
			$companion = Companion::whereReservationId($id)->count();
			for ($i = 0; $i < $companion; $i++) {
				$c_delete = Companion::whereReservationId($id)->first();
				$c_delete->delete();
			}
			$c = new Companion;
			$c->reservation_id = $reservation->id;
			$c->client_id = $inputs['reservation']['client_id'];
			$c->save();
			foreach ($inputs['companions'] as $key => $companion) {
				if(isset($companion['client_id'])) {
					$buscar = Companion::whereClientId($companion['client_id'])->first();
					if(is_null($buscar)) {
						$c = new Companion;
						$c->reservation_id = $reservation->id;
						$c->client_id = $companion['client_id'];
						$c->save();
					}
				}elseif(isset($companion['id'])){
					$buscar = Companion::whereClientId($companion['id'])->first();
					if(is_null($buscar)) {
						$c = new Companion;
						$c->reservation_id = $reservation->id;
						$c->client_id = $companion['id'];
						$c->save();
					}
				}
			}
			return response()->json([
				'error' => false,
				'message' => 'Se ha guardado la reserva.',
			], 200);
		}else{
			return response()->json([
				'error' => true,
				'message' => 'Error al guardar la categoria.',
			], 404);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//$reservation = Reservation::findOrFail($id);
		$reservation = Reservation::join("clients", function($join){
			$join->on("clients.id","=","reservations.client_id");
		})->join("prices", function($join){
			$join->on("prices.id","=","reservations.price_id");
		})->join("accommodation", function($join){
			$join->on("accommodation.id","=","reservations.accommodation_id");
		})
			->where("reservations.id",$id)
			->first([
			'reservations.*',
			'clients.name as client_name',
			'clients.lastname1 as client_lastname1',
			'clients.lastname2 as client_lastname2',
			'prices.name as price_name',
			'prices.value as price_value',
			'accommodation.name as cabana_name'
		]);

		return $reservation->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$reservation = Reservation::findOrFail($id);
		$companion = Companion::whereReservationId($id)->count();
		if($reservation->delete()){
			for($i=0;$i<$companion;$i++){
				$c_delete = Companion::whereReservationId($id)->first();
				$c_delete->delete();
			}
			return response()->json([
				'error' => false,
				'message' => 'Registro eliminado con exito.',
			], 200);
		}

		return response()->json([
			'error' => true,
			'message' => 'Error al eliminar el registro.',
		], 404);
	}

	public function checkin($id)
	{
		$reservation = Reservation::findOrFail($id);
		$reservation->status = 'CHECKIN';
		$reservation->backgroundColor = 'FORESTGREEN';
		$reservation->save();
	}

	public function checkout($id)
	{
		$reservation = Reservation::findOrFail($id);
		$reservation->status = 'CHECKOUT';
		$reservation->backgroundColor = 'DARKRED';
		$reservation->save();
	}

	public function cancelar($id)
	{
		$reservation = Reservation::findOrFail($id);
		$reservation->status = 'CANCELADA';
		$reservation->backgroundColor = 'DARKGREY';
		$reservation->save();
	}

	public function consumos(Request $request, $id){
		$inputs = $request->all();
		//return json_encode($inputs);

		foreach ($inputs['consumos'] as $key => $consumo) {
			if(isset($consumo['cantidad'])){//Verifica si hay cantidad registrada, sino NO la guarda en la DB
				$c = new Consumo;
				$c->reservation_id = $id;
				$c->item_id = $consumo['id'];
				$c->cantidad = $consumo['cantidad'];
				$c->save();
			}
		}
	}

	public function getconsumos($id){
		$consumos = Consumo::join("items", function($join){
			$join->on("items.id","=","consumos.item_id");
		})
			->where("consumos.reservation_id","=",$id)
			->get([
				"consumos.*",
				"items.value as item_value",
				"items.name as item_name"
			]);
		return $consumos->toJson();
	}

	public function validateout($date_in, $date_out){
		$validating_date = Reservation::join("accommodation", function($join){
			$join->on("reservations.accommodation_id","=","accommodation.id");
		})->where("reservations.status","=","CHECKIN")
		->where("reservations.status","=","PENDIENTE")
		->whereBetween("reservations.date_in",[$date_in, $date_out])
		->orWhereBetween("reservations.date_out",[$date_in, $date_out])
			->get([
				"reservations.accommodation_id as accommodation_id",
				"reservations.date_in",
				"reservations.date_out",
				"reservations.status",
				"accommodation.name",
			]);//Obtengo las Cabañas Ocupadas

		//$cabanas_disponibles['1']->id; Asi accedo a los valores del vector que me retorna el query
		//$cabanas_disponibles = Cabana::get()->except(9);
		$cabanas_disponibles = Cabana::whereNotIn('id',
			$validating_date->lists('accommodation_id'))//lists me da un Array con el parámetro que le indique
			->get([
			"accommodation.*",
			"accommodation.id as cabana_id"]);//Retorna las Cabañas Libres en las Fechas Buscadas! Por fiiiin!!!!
		
		return json_encode($cabanas_disponibles);
	}
	//END TEST VALIDATE TODAS LAS FECHAS

}
