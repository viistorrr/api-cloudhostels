<?php namespace App\Http\Controllers;

use App\Rol;
use Illuminate\Http\Request;
class RolController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $roles = Rol::get();

    return $roles->toJson();//retorna todos lo que viene de la tabla de la DB
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $s = new Rol;
    $s->fill($request->all());

    if($s->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el registro.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el registro.',
      ], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $category = Rol::findOrFail($id);
    $category->fill($request->only(['name']));

    if($category->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $category = Rol::findOrFail($id);

    if($category->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }

}

?>