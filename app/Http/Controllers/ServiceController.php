<?php namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
class ServiceController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $service = Service::get();
    return $service->toJson();//retorna todos lo que viene de la tabla de la DB
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $service = new Service;
      $service->fill($request->all());

    if($service->save()){
      return response()->json([
          'error' => false,
          'message' => 'Se ha guardado el servicio.',
      ], 200);
    }else{
      return response()->json([
          'error' => true,
          'message' => 'Error al guardar el servicio.',
      ], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $service = Service::findOrFail($id);
      $service->fill($request->only('name','value'));

    if($service->save()){
        return response()->json([
            'error' => false,
            'message' => 'Registro guardado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al guardar el registro.',
    ], 404);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $service = Service::findOrFail($id);

    if($service->delete()){
        return response()->json([
            'error' => false,
            'message' => 'Registro eliminado con exito.',
        ], 200);
    }

    return response()->json([
        'error' => true,
        'message' => 'Error al eliminar el registro.',
    ], 404);
  }

}

?>