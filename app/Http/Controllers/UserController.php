<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use JWT;
use App\User;

class UserController extends Controller {

    /**
     * Generate JSON Web Token.
     */
    protected function createToken($user)
    {
        $payload = [
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60)
        ];
        return JWT::encode($payload, Config::get('app.token_secret'));
    }

    public function index()
    {
        $users = User::join("roles", function($join){
            $join->on("roles.id","=","users.rol");
        })->get([
            "users.*",
            "roles.name as role_name"
        ]);

        return $users->toJson();//retorna todos usuarios que estan registrados en el sistema
    }

    /**
     * Get signed in user's profile.
     */
    public function getUser(Request $request)
    {
        $user = User::join("roles", function($join){
            $join->on("roles.id","=","users.rol");
        })->where("users.id","=",$request['user']['sub'])
            ->first([
                "users.*",
                "roles.name as rol_name"
            ]);

        return $user;
    }

    /**
     * Update signed in user's profile.
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validEmail = false;
        $email = $request->input('email');

        //Check User Email
        if( isset($email) && ($user->email == $email)){
            $validEmail = true;
        }else if( isset($email) && (User::where('email', $email)->count() == 0)  ){
            $validEmail = true;
        }else{
            $validEmail = false;
        }

        if($validEmail){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->password = \Hash::make($request->input('password'));

            if($user->save()){
                $token = $this->createToken($user);
                return response()->json([
                    'error' => false,
                    'message' => ' Has actualizado tu información correctamente!',
                    'token' => $token
                ], 200);
            }else{
                return response()->json([
                    'error' => true,
                    'message' => 'Error al guardar el perfil.'
                ], 500);
            }
        }else{
            return response()->json([
                'error' => true,
                'message' => 'El correo ya está registrado en SE LE LAVA!.'
            ], 500);
        }
    }

    public function store(Request $request)
    {

        $user = new User;
        $validEmail = false;
        $email = $request->input('email');

        //Check User Email
        if( isset($email) && ($user->email == $email)){
            $validEmail = true;
        }else if( isset($email) && (User::where('email', $email)->count() == 0)  ){
            $validEmail = true;
        }else{
            $validEmail = false;
        }

        if($validEmail){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->rol = $request->input('rol');
            $user->phone = $request->input('phone');
            $user->password = \Hash::make($request->input('password'));

            if($user->save()){
                $token = $this->createToken($user);
                return response()->json([
                    'error' => false,
                    'message' => ' Has actualizado tu información correctamente!',
                    'token' => $token
                ], 200);
            }else{
                return response()->json([
                    'error' => true,
                    'message' => 'Error al guardar el perfil.'
                ], 500);
            }
        }else{
            return response()->json([
                'error' => true,
                'message' => 'El correo ya está registrado en CLOUD HOSTELS.'
            ], 500);
        }
    }
}