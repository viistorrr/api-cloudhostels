<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// OAuth, Login and Signup Routes.
Route::post('auth/twitter', 'AuthController@twitter');
Route::post('auth/facebook', 'AuthController@facebook');
Route::post('auth/foursquare', 'AuthController@foursquare');
Route::post('auth/instagram', 'AuthController@instagram');
Route::post('auth/github', 'AuthController@github');
Route::post('auth/google', 'AuthController@google');
Route::post('auth/linkedin', 'AuthController@linkedin');
Route::post('auth/login', 'AuthController@login');
Route::get('auth/unlink/{provider}', ['middleware' => 'auth', 'uses' => 'AuthController@unlink']);

Route::get('/', ['uses' => 'HomeController@index']);

//Login and Signup Routes.

Route::post('auth/login', 'AuthController@login');
Route::any('auth/signup', 'AuthController@signup');

// API Routes.
Route::get('api/me', ['middleware' => 'auth', 'uses' => 'UserController@getUser']);
Route::put('api/me', ['middleware' => 'auth', 'uses' => 'UserController@updateUser']);

/* Workers */
Route::get('api/workers', ['middleware' => 'auth', 'uses' => 'WorkerController@index']);
Route::post('api/worker', ['middleware' => 'auth', 'uses' => 'WorkerController@add']);
Route::get('api/worker/{id}', ['middleware' => 'auth', 'uses' => 'WorkerController@get']);
Route::put('api/worker/edit/{id}', ['middleware' => 'auth', 'uses' => 'WorkerController@edit']);
Route::post('api/upload/worker', ['uses' => 'WorkerController@upload']);