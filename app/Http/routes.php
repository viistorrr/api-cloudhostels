<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// OAuth, Login and Signup Routes.
Route::post('auth/twitter', 'AuthController@twitter');
Route::post('auth/facebook', 'AuthController@facebook');
Route::post('auth/foursquare', 'AuthController@foursquare');
Route::post('auth/instagram', 'AuthController@instagram');
Route::post('auth/github', 'AuthController@github');
Route::post('auth/google', 'AuthController@google');
Route::post('auth/linkedin', 'AuthController@linkedin');
Route::post('auth/login', 'AuthController@login');
Route::get('auth/unlink/{provider}', ['middleware' => 'auth', 'uses' => 'AuthController@unlink']);

Route::get('/', ['uses' => 'HomeController@index']);

//Login and Signup Routes.

Route::post('auth/login', 'AuthController@login');
Route::post('auth/signup', 'AuthController@signup');

// API Routes.
Route::get('api/me', ['middleware' => 'auth', 'uses' => 'UserController@getUser']);
Route::put('api/me', ['middleware' => 'auth', 'uses' => 'UserController@updateUser']);

/* Business */
Route::get('categories/{id}', ['uses' => 'BusinessController@byCategory']);
// Load Points.
Route::post('load', ['middleware' => 'auth', 'uses' => 'BusinessController@load']);
Route::get('points', ['middleware' => 'auth', 'uses' => 'BusinessController@getPoints']);
//Route::get('history', ['middleware' => 'auth', 'uses' => 'TransactionController@index']);
Route::get('ranking', ['middleware' => 'auth', 'uses' => 'UserController@ranking']);

/* UPLOAD */
//Route::post('upload/profile', ['middleware' => 'auth', 'uses' => 'UploadController@profilePicture']);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 * Módulo de Usuarios
 */
Route::resource('roles', 'RolController');//Roles del Sistema
Route::resource('user', 'UserController');//Usuarios en el Sistema
/*
 * Módulo de Alojamiento
 */
Route::resource('prices', 'PriceController');//Tarifas ofrecidas por el Hotel(Pensión completa, dia de sol, etc)
Route::resource('habitaciones', 'CabanaController');//Cabañas en el Hotel
Route::get('cabana-date-in/{id}', 'CabanaController@cabanadatein');//Selecciona las cabañas que NO estan ocupadas en la Fecha de Entrada
Route::get('cabana-date-out/{id}', 'CabanaController@cabanadateout');//Selecciona las cabañas que NO estan ocupadas en la Fecha de Entrada
Route::resource('validating-date-in', 'ReservationController@validatein');//Cabañas en el Hotel
Route::get('reservation/validating-date-out/{date_in}/{date_out}', 'ReservationController@validateout');//Cabañas en el Hotel
Route::resource('reservation', 'ReservationController');//Listado de Reservas registradas en el sistema
Route::resource('events', 'DashboardController');//Reservas mostradas en el Calendario del sistema
Route::resource('today', 'DashboardController@today');//Reservas mostradas en el Calendario del sistema
Route::resource('companion', 'CompanionController');//Mostrar Acompañantes en la ventana modal de las reservas registradas en el Calendario
Route::post('checkin/{id}', ['middleware' => 'auth', 'uses' => 'ReservationController@checkin']);//Realizar CheckIN
Route::post('checkout/{id}', ['middleware' => 'auth', 'uses' => 'ReservationController@checkout']);//Realizar CheckOUT
Route::post('cancelar/{id}', ['middleware' => 'auth', 'uses' => 'ReservationController@cancelar']);//Cancelar Reserva
Route::post('consumos/{id}', ['middleware' => 'auth', 'uses' => 'ReservationController@consumos']);//Registrar Consumos en la Reserva
Route::get('getconsumos/{id}', 'ReservationController@getconsumos');//Registrar Consumos en la Reserva

/*
Rutas Pruebas Unitarias
*/

Route::get('insertartest', 'ClientTestController@insertarTest');//Ejecutar Prueba
Route::get('eliminartest', 'ClientTestController@eliminarTest');//Ejecutar Prueba
Route::get('insertarhabitacion', 'HabitacionTestController@insertarHabitacion');//Ejecutar Prueba
Route::get('eliminarhabitacion', 'HabitacionTestController@eliminarHabitacion');//Ejecutar Prueba

/*
 * Módulo de Gestión
 */
Route::resource('item', 'ItemController');//Productos ofrecidos en el Hotel(Botella de vino,etc)
Route::resource('service', 'ServiceController');//Servicios ofrecidos por el Hotel(Transporte, etc)

/*
 * Módulo de Clientes
 */
Route::resource('client', 'ClientController');//Editar un cliente
Route::resource('clients', 'ClientController');//Listado de Clientes
Route::resource('country', 'CountryController');//Listado de Países
Route::resource('city', 'CityController');//Listado de Ciudades

/*
 * Reportes
 */
Route::resource('sire', 'DashboardController@sire');//SIRE
Route::resource('revenuemanagement', 'DashboardController@revenuemanagement');//GUARDAR R
Route::resource('getrevenuemanagement', 'DashboardController@getrevenuemanagement');//REVENUE MANAGEMENT