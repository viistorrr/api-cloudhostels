<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model {

	protected $table = 'prices';
	public $timestamps = true;
	protected $fillable = array('id','name','value');

}