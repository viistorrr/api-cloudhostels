<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {

	protected $table = 'reservations';
	public $timestamps = true;

	public function companions(){
		return $this->hasMany('App\Companion');
	}

	public function cabanas(){
		return $this->hasMany('App\Cabana');
	}

	public function client(){
		return $this->belongsTo('App\Client');
	}

	protected $fillable = ['client_id',
		'date_in',
		'date_out',
		'cabana_id',
		'price_id',
		'abono',
		'status',
		'annotations'];

}
