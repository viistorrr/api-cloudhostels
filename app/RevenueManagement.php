<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueManagement extends Model {

	protected $table = 'revenue_management';
	public $timestamps = true;

	protected $fillable = [
		'fecha',
		'total_cabanas_disponibles',
		'cabanas_ocupadas',
		'cabanas_para_ocupar',
		'huespedes_alojados',
		'tarifa',
		'venta_hab',
		'porc_ocupacion',
		'i_alojamiento',
		'tarifa_prom_cabana',
		'rev_v_par'];

}
