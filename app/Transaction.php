<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $table = 'transactions';
	public $timestamps = true;

    protected $fillable = array('business_id', 'user_id', 'type', 'bill', 'amount', 'points');

    public function business(){
        return $this->belongsTo('App\Business');
    }

    public function getCreatedAtAttribute($value)
    {
        $value = date('U', strtotime($value));
        return $value * 1000;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value = date('U', strtotime($value));
        return $value * 1000;
    }

}