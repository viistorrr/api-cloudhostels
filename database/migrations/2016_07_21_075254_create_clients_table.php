<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->string('lastname');
		    $table->string('document');
		    $table->string('phone');
		    $table->string('email')->unique();
		    $table->string('occupation');
		    $table->string('address');
		    $table->date('birthday');

		    $table->integer('country_id')->unsigned();
		    $table->foreign('country_id')->references('id')->on('countries');

		    /*$table->integer('city_id')->unsigned();
		    $table->foreign('city_id')->references('id')->on('cities');*/


		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
