<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');

		App\User::create(array(
			'name' => 'Eli José Carrasquero',
			'picture' => 'image.jpg',
			'email' => 'ielijose@gmail.com',
			'password' => \Hash::make('2512'),
			'type' => 'admin'
			));

		App\User::create(array(
			'name' => 'Victor Meza',
			'picture' => 'image.jpg',
			'email' => 'victormeza41@gmail.com',
			'password' => \Hash::make('1234'),
			'type' => 'admin'
		));
	}

}
